using System;
using System.Drawing;

namespace AdventOfCode2018.days {
    public class Day11 : IDay {
        public int NumberOfDay { get; } = 11;
        private readonly string[] input;

        public Day11(string[] input) {
            this.input = input;
        }

        public object Task1() {
            int serialNumber = Convert.ToInt32(input[1]) % 10;

            Grid maxGrid = null;
            for (int i = 1; i <= 298; i++) {
                for (int j = 1; j <= 298; j++) {
                    var currentGrind = new Grid(i, j, serialNumber, 3);
                    if (maxGrid == null || maxGrid.PowerLevel < currentGrind.PowerLevel)
                        maxGrid = currentGrind;
                }
                
            }
            

            return maxGrid.ToString1();
        }

        public object Task2() {
            int serialNumber = Convert.ToInt32(input[0]);

            Grid maxGrid = null;
            for (int size = 1; size <= 300; size++) {
                for (int i = 1; i <= 301 - size; i++) {
                    for (int j = 1; j <= 301 - size; j++) {
                        var currentGrind = new Grid(i, j, serialNumber, size);
                        if (maxGrid == null || maxGrid.PowerLevel < currentGrind.PowerLevel) {
                            maxGrid = currentGrind;
                            //Console.Out.WriteLine(maxGrid);
                        }
                    }
                }
            }


            return maxGrid.ToString2();
        }
        
        private class Grid {
            public Point TopLeft { get; }
            public int PowerLevel { get; }
            public int Size { get; }
            private readonly int serialNumber;
            

            public Grid(int x, int y, int serialNumber, int size) {
                TopLeft = new Point(x, y);
                this.serialNumber = serialNumber;
                Size = size;

                for (int i = x; i < x + size; i++) {
                    for (int j = y; j < y + size; j++) {
                        PowerLevel += CalculatePowerLevel(i, j);
                    }
                }
            }

            private int CalculatePowerLevel(int x, int y) {
                int rackId = x + 10;
                int powerLevel = rackId * y;
                powerLevel += serialNumber;
                powerLevel *= rackId;
                powerLevel = (powerLevel / 100) % 10;
                return powerLevel - 5;
            }

            public string ToString1() {
                return $"{TopLeft.X},{TopLeft.Y}";
            }

            public string ToString2() {
                return $"{TopLeft.X},{TopLeft.Y},{Size}";
            }

            public override string ToString() {
                return $"{nameof(TopLeft)}: {TopLeft}, {nameof(PowerLevel)}: {PowerLevel}, {nameof(Size)}: {Size}";
            }
        }
    }
}