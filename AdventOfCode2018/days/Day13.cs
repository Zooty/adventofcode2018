using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using AdventOfCode2018.extensions;

namespace AdventOfCode2018.days {
    public class Day13 : IDay {
        public int NumberOfDay { get; } = 13;
        private string[] input;

        public Day13(string[] input) {
            this.input = input;
        }

        public object Task1() {
            char[][] map = input.Select((s, i) => s.ToCharArray()).ToArray();
            

            List<Car> cars = new List<Car>();
            for (var x = 0; x < map.Length; x++) {
                for (var y= 0; y < map[x].Length; y++) {
                    if (map[x][y] == '^') {
                        cars.Add(new Car(map, y, x, Car.Direction.Up));
                        map[x][y] = '|';
                    }
                    if (map[x][y] == 'v') {
                        cars.Add(new Car(map, y, x, Car.Direction.Down));
                        map[x][y] = '|';
                    }
                    if (map[x][y] == '>') {
                        cars.Add(new Car(map, y, x, Car.Direction.Right));
                        map[x][y] = '-';
                    }
                    if (map[x][y] == '<') {
                        cars.Add(new Car(map, y, x, Car.Direction.Left));
                        map[x][y] = '-';
                    }
                }
            }

            while (true) {
                cars = cars.OrderBy(car => car.Y).ThenBy(car => car.X).ToList();
                foreach (Car car in cars) {
                    car.Go(); 
                    var collide = Collide(cars);
                    if (collide != null)
                        return collide;
                }
            }
        }

        public object Task2() {
            char[][] map = input.Select((s, i) => s.ToCharArray()).ToArray();

            List<Car> cars = new List<Car>();
            for (var x = 0; x < map.Length; x++) {
                for (var y= 0; y < map[x].Length; y++) {
                    if (map[x][y] == '^') {
                        cars.Add(new Car(map, y, x, Car.Direction.Up));
                        map[x][y] = '|';
                    }
                    if (map[x][y] == 'v') {
                        cars.Add(new Car(map, y, x, Car.Direction.Down));
                        map[x][y] = '|';
                    }
                    if (map[x][y] == '>') {
                        cars.Add(new Car(map, y, x, Car.Direction.Right));
                        map[x][y] = '-';
                    }
                    if (map[x][y] == '<') {
                        cars.Add(new Car(map, y, x, Car.Direction.Left));
                        map[x][y] = '-';
                    }
                }
            }

            while (true) {
                HashSet<Car> collided = new HashSet<Car>();
                cars = cars.OrderBy(car => car.Y).ThenBy(car => car.X).ToList();
                foreach (Car car in cars) {
                    car.Go();
                    Collide(cars, collided);
                    
                    if (collided.Count == 2 && cars.Count == 3)
                        return cars.First(car1 => !collided.Contains(car1));
                }
                
//                if(collided.Count > 0)
//                    Console.Out.WriteLine(collided.Last());
                foreach (Car car in collided) {
                    cars.Remove(car);
                }

                if (cars.Count == 1)
                    return (cars[0].X, cars[0].Y);
                if(cars.Count == 0)
                    throw new Exception("wefwefwefsdfweagewgdfgs");
            }
        }

        private string Collide(IReadOnlyList<Car> cars, HashSet<Car> collided = null) {
            for (var i = cars.Count - 1; i >= 0; i--) {
                for (var j = cars.Count - 1; j >= 0; j--) {
                    if (cars[i] != cars[j] && 
                        (cars[i].X == cars[j].X && cars[i].Y == cars[j].Y //|| 
                            //cars[i].CarDirection == Car.Direction.Left && cars[j].CarDirection == Car.Direction.Right && cars[i].Y == cars[j].Y && cars[i].X - 1 == cars[j].X ||
                            //cars[i].CarDirection == Car.Direction.Up && cars[j].CarDirection == Car.Direction.Down && cars[i].X == cars[j].X && cars[i].Y - 1 == cars[j].Y
                        )) {
                        if (collided != null) {
                            collided.Add(cars[i]);
                            collided.Add(cars[j]);
                        }
                            
                        return $"{cars[i].X},{cars[j].Y}";
                    }
                }
            }

            return null;
        }

        private void print(char[][] map, List<Car> cars) {
            for (int y = 0; y < map.Length; y++) {
                for (int x = 0; x < map[y].Length; x++) {
                    Car car = cars.FirstOrDefault(c => c.X == x && c.Y == y);
                    
                    Console.Write(car?.Dir() ?? map[y][x]);
                } Console.WriteLine();
            }
        }
    }
    
    public class Car {
        private readonly char[][] map;
        public int X { get; set; }
        public int Y { get; set; }
        public Direction CarDirection { get; set; }
        public Turn NextTurn { get; set; } = Turn.Left;

        public Car(char[][] map, int x, int y, Direction direction) {
            this.map = map;
            X = x;
            Y = y;
            CarDirection = direction;
        }

        public void Go() {
            char nextPath;
            switch (CarDirection) {
                case Direction.Up:
                    nextPath = map[Y - 1][X];
                    if (nextPath == '\\')
                        CarDirection = Direction.Left;
                    if (nextPath == '/')
                        CarDirection = Direction.Right;
                    if (nextPath == '+') {
                        switch (NextTurn) {
                            case Turn.Left:
                                CarDirection = Direction.Left;
                                NextTurn++;
                                break;

                            case Turn.Straight:
                                NextTurn++;
                                break;
                            case Turn.Right:
                                CarDirection = Direction.Right;
                                NextTurn = Turn.Left;
                                break;
                        }
                    }

                    Y--;
                    break;
                case Direction.Down:
                    nextPath = map[Y + 1][X];
                    if (nextPath == '/')
                        CarDirection = Direction.Left;
                    if (nextPath == '\\')
                        CarDirection = Direction.Right;
                    if (nextPath == '+') {
                        switch (NextTurn) {
                            case Turn.Left:
                                CarDirection = Direction.Right;
                                NextTurn++;
                                break;
                            case Turn.Straight:
                                NextTurn++;
                                break;
                            case Turn.Right:
                                CarDirection = Direction.Left;
                                NextTurn = Turn.Left;
                                break;
                        }
                    }

                    Y++;
                    break;
                case Direction.Right:
                    nextPath = map[Y][X + 1];
                    if (nextPath == '/')
                        CarDirection = Direction.Up;
                    if (nextPath == '\\')
                        CarDirection = Direction.Down;
                    if (nextPath == '+') {
                        switch (NextTurn) {
                            case Turn.Left:
                                CarDirection = Direction.Up;
                                NextTurn++;
                                break;
                            case Turn.Straight:
                                NextTurn++;
                                break;
                            case Turn.Right:
                                CarDirection = Direction.Down;
                                NextTurn = Turn.Left;
                                break;
                        }
                    }

                    X++;
                    break;
                case Direction.Left:
                    nextPath = map[Y][X - 1];
                    if (nextPath == '/')
                        CarDirection = Direction.Down;
                    if (nextPath == '\\')
                        CarDirection = Direction.Up;
                    if (nextPath == '+') {
                        switch (NextTurn) {
                            case Turn.Left:
                                CarDirection = Direction.Down;
                                NextTurn++;
                                break;
                            case Turn.Straight:
                                NextTurn++;
                                break;
                            case Turn.Right:
                                CarDirection = Direction.Up;
                                NextTurn = Turn.Left;
                                break;
                        }
                    }

                    X--;
                    break;
            }
        }

        public char Dir() {
            switch (CarDirection) {
                case Direction.Up:
                    return '^';
                case Direction.Down:
                    return 'v';
                case Direction.Right:
                    return '>';
                case Direction.Left:
                    return '<';
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public enum Direction {
            Up,
            Down,
            Right,
            Left 
        }
        
        public enum Turn {
            Left = 0,
            Straight = 1,
            Right = 2
        }

        public override string ToString() {
            return $"{X},{Y}";
        }
    }
}