using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018.days {
    public class Day3 : IDay{
        public int NumberOfDay { get; } = 3;
        
        private readonly string[] input;
        private List<Claim> claims;

        public Day3(string[] input) {
            this.input = input;
        }

        private void parseInput() {
            var regex = new Regex(@"#(?<id>\d+)\s@\s(?<left>\d+),(?<top>\d+):\s(?<width>\d+)x(?<height>\d+)");
            claims = new List<Claim>();
            foreach (var s in input) {
                var match = regex.Match(s);
                if(!match.Success)
                    throw new FormatException($"Pattern is wrong for {s}");

                claims.Add(new Claim(
                    int.Parse(match.Groups["id"].Value),
                    int.Parse(match.Groups["left"].Value),
                    int.Parse(match.Groups["top"].Value),
                    int.Parse(match.Groups["width"].Value),
                    int.Parse(match.Groups["height"].Value)));
            }
        }

        public object Task1() {
            if(claims == null)
                parseInput();
            
            var fabrics = new HashSet<Point>();
            
            for (var i = 0; i < claims.Count; i++) {
                for (var j = 0; j < i; j++) {
                    var overlaps = claims[i].Overlap(claims[j]);
                    foreach (var overlap in overlaps) {
                        fabrics.Add(overlap);
                    }
                }
            }

            return fabrics.Count;
        }

        public object Task2() {
            if(claims == null)
                parseInput();

            var possibleClaims = new HashSet<Claim>(claims);
            for (var i = 0; i < claims.Count; i++) {
                for (var j = 0; j < i; j++) {
                    var overlaps = claims[i].Overlap(claims[j]);
                    if (overlaps.Count != 0) {
                        possibleClaims.Remove(claims[i]);
                        possibleClaims.Remove(claims[j]);
                    }
                }
            }

            if (possibleClaims.Count != 1) {
                throw new Exception("Not only one non-overlapping claim was found!");
            }

            return possibleClaims.First().Id;
        }

        private class Claim {
            public int Id { get; }
            public int Left { get; }
            public int Top { get;}
            public int Width { get; }
            public int Height { get; }

            public HashSet<Point> Fabrics { get; } = new HashSet<Point>();


            public Claim(int id, int left, int top, int width, int height) {
                Id = id;
                Left = left;
                Top = top;
                Width = width;
                Height = height;

                for (int x = left + 1; x <= width + left; x++) {
                    for (int y = top + 1; y <= height + top; y++) {
                        Fabrics.Add(new Point(x, y));
                    }
                }
            }

            public List<Point> Overlap(Claim other) {
                var overlap = new List<Point>();
                foreach (var fabric in Fabrics) {
                    if(other.Fabrics.Contains(fabric))
                        overlap.Add(fabric);
                }

                return overlap;
            }
        }  
    }
}