using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2018.days {
    public class Day9 : IDay {
        public int NumberOfDay { get; } = 9;

        private readonly string[] input;

        public Day9(string[] input) {
            this.input = input;
        }


        public object Task1() {
            string[] inputSplitString = input[0].Split(" ");
            int players = Convert.ToInt32(inputSplitString[0]);
            int maxMarble = Convert.ToInt32(inputSplitString[6]);

            var scoreBoard = new Dictionary<int, int>();

            var marbles = new LinkedList<int>();

            marbles.AddFirst(0);
            LinkedListNode<int> currentMarble = marbles.First;


            int currentPlayer = 0;
            for (int i = 1; i <= maxMarble; i++) {
                if (i % 23 == 0) {
                    if (!scoreBoard.ContainsKey(currentPlayer)) 
                        scoreBoard.Add(currentPlayer, 0);
                    scoreBoard[currentPlayer] += i;
                    LinkedListNode<int> removedMarble = currentMarble.Previous ?? marbles.Last;
                    for (int j = 0; j < 6; j++) {
                        removedMarble = removedMarble.Previous ?? marbles.Last;
                        
                    }
                    
                    currentMarble = removedMarble.Next;
                    scoreBoard[currentPlayer] += removedMarble.Value;
                    marbles.Remove(removedMarble);
                }
                else {
                    currentMarble = marbles.AddAfter(currentMarble.Next ?? marbles.First, i);
                }

                currentPlayer++;
                if (currentPlayer == players)
                    currentPlayer = 0;
            }


            return scoreBoard.OrderByDescending(pair => pair.Value).First().Value;
        }

        public object Task2() {
            string[] inputSplitString = input[0].Split(" ");
            int players = Convert.ToInt32(inputSplitString[0]);
            int maxMarble = Convert.ToInt32(inputSplitString[6]) * 100;

            var scoreBoard = new Dictionary<int, long>();

            var marbles = new LinkedList<int>();

            marbles.AddFirst(0);
            LinkedListNode<int> currentMarble = marbles.First;


            int currentPlayer = 0;
            for (int i = 1; i <= maxMarble; i++) {
                if (i % 23 == 0) {
                    if (!scoreBoard.ContainsKey(currentPlayer)) 
                        scoreBoard.Add(currentPlayer, 0);
                    scoreBoard[currentPlayer] += i;
                    LinkedListNode<int> removedMarble = currentMarble.Previous ?? marbles.Last;
                    for (int j = 0; j < 6; j++) {
                        removedMarble = removedMarble.Previous ?? marbles.Last;
                        
                    }
                    
                    currentMarble = removedMarble.Next;
                    scoreBoard[currentPlayer] += removedMarble.Value;
                    marbles.Remove(removedMarble);
                }
                else {
                    currentMarble = marbles.AddAfter(currentMarble.Next ?? marbles.First, i);
                }

                currentPlayer++;
                if (currentPlayer == players)
                    currentPlayer = 0;
            }


            return scoreBoard.OrderByDescending(pair => pair.Value).First().Value;
        }
    }
}