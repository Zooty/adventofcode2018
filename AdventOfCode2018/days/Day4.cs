using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2018.days {
	public class Day4 : IDay {
		public int NumberOfDay { get; } = 4;
		private readonly string[] input;

		public Day4(string[] input) {
			this.input = input;
		}

		public object Task1() {
			var events = new List<Event>();
			foreach (var s in input) {
				string dateString = s.Substring(1, 16);
				var eventStrings = s.Substring(19).Split(" ");
				switch (eventStrings[0]) {
					case "Guard":
						events.Add(new Event(DateTime.Parse(dateString), Event.EventType.Start){Id = Convert.ToInt32(eventStrings[1].Substring(1))});
						break;
					case "falls":
						events.Add(new Event(DateTime.Parse(dateString), Event.EventType.Sleep));
						break;
					case "wakes":
						events.Add(new Event(DateTime.Parse(dateString), Event.EventType.Wake));
						break;
					default:
						throw new ArgumentException();
				}
			}

			events = events.OrderBy(e => e.time).ToList();
			int previousID = -1;
			foreach (var ev in events) {
				if (ev.Type == Event.EventType.Start) {
					previousID = ev.Id;
				}
				else {
					if(previousID == -1)
						throw new ArgumentException();
					ev.Id = previousID;
				}
			}

			var minutesSpentSleeping = new Dictionary<int, List<int>>();
			var groupedEvents = events.GroupBy(data => data.Id).ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());
			foreach (var key in groupedEvents.Keys) {
				for (var index = 0; index < groupedEvents[key].Count; index++) {
					var ev = groupedEvents[key][index];
					if (ev.Type == Event.EventType.Sleep) {
						var wakeEvent = groupedEvents[key][index + 1];
						int minutesSlept = (int)(wakeEvent.time - ev.time).TotalMinutes;
						if(!minutesSpentSleeping.ContainsKey(ev.Id))
							minutesSpentSleeping.Add(ev.Id, new List<int>());
						for (int minute = 0; minute < minutesSlept; minute++) {
							minutesSpentSleeping[ev.Id].Add(ev.time.AddMinutes(minute).Minute);
						}
					}
				}
			}

			int sleepiestId = minutesSpentSleeping.ToDictionary(pair => pair.Key, pair => pair.Value.Count).OrderByDescending(pair => pair.Value).First().Key;
			int sleepiestMinute = minutesSpentSleeping[sleepiestId].GroupBy(i => i)
				.ToDictionary(integers => integers.Key, integers => integers.ToList().Count)
				.OrderByDescending(pair => pair.Value).First().Key;


			return sleepiestId * sleepiestMinute;
		}

		public object Task2() {
			var events = new List<Event>();
			foreach (var s in input) {
				string dateString = s.Substring(1, 16);
				var eventStrings = s.Substring(19).Split(" ");
				switch (eventStrings[0]) {
					case "Guard":
						events.Add(new Event(DateTime.Parse(dateString), Event.EventType.Start){Id = Convert.ToInt32(eventStrings[1].Substring(1))});
						break;
					case "falls":
						events.Add(new Event(DateTime.Parse(dateString), Event.EventType.Sleep));
						break;
					case "wakes":
						events.Add(new Event(DateTime.Parse(dateString), Event.EventType.Wake));
						break;
					default:
						throw new ArgumentException();
				}
			}

			events = events.OrderBy(e => e.time).ToList();
			int previousID = -1;
			foreach (var ev in events) {
				if (ev.Type == Event.EventType.Start) {
					previousID = ev.Id;
				}
				else {
					if(previousID == -1)
						throw new ArgumentException();
					ev.Id = previousID;
				}
			}

			var minutesSpentSleeping = new List<(int, int)>();
			var groupedEvents = events.GroupBy(data => data.Id).ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());
			foreach (var key in groupedEvents.Keys) {
				for (var index = 0; index < groupedEvents[key].Count; index++) {
					var ev = groupedEvents[key][index];
					if (ev.Type == Event.EventType.Sleep) {
						var wakeEvent = groupedEvents[key][index + 1];
						int minutesSlept = (int)(wakeEvent.time - ev.time).TotalMinutes;
						for (int minute = 0; minute < minutesSlept; minute++) {
							minutesSpentSleeping.Add((ev.Id, ev.time.AddMinutes(minute).Minute));
						}
					}
				}
			}

			(int minuteValue, (int sleepyId, int _)) = minutesSpentSleeping.GroupBy(i => i.Item2).ToDictionary(i => i.Key, i => i.ToList())
				.ToDictionary(i => i.Key,
					i => i.Value.GroupBy(g => g.Item1).ToDictionary(v => v.Key, v => v.ToList().Count)
						.OrderByDescending(x => x.Value).First()).OrderByDescending(k => k.Value.Value).First();

			return minuteValue * sleepyId;
		}

		private class Event {
			public int Id { get; set; }
			public DateTime time { get; }
			public EventType Type { get; }

			public Event(DateTime time, EventType type) {
				this.time = time;
				Type = type;
			}

			public override string ToString() {
				return $"{nameof(Id)}: {Id}, {nameof(time)}: {time}, {nameof(Type)}: {Type}";
			}

			public enum EventType {
				Start,
				Sleep,
				Wake
			}
		}
	}
}