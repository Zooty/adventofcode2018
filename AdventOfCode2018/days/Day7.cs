using System.Collections.Generic;
using System.Linq;
using AdventOfCode2018.extensions;

namespace AdventOfCode2018.days {
    public class Day7 : IDay {
        private string[] input;

        public Day7(string[] input) {
            this.input = input;
        }

        public int NumberOfDay { get; } = 7;

        private List<(char pre, char post)> steps = null;
        
        public object Task1() {
            steps = input.Select(s => (s[5], s[36])).ToList();
            List<char> order = new List<char>();

            HashSet<char> startInstructions = new HashSet<char>();
            foreach (var tuple in steps) {
                startInstructions.Add(tuple.pre);
                startInstructions.Add(tuple.post);
            }

            startInstructions = startInstructions.OrderBy(x => x).ToHashSet();

            while (startInstructions.Any()) {
                var valid = startInstructions.First(s => steps.All(d => d.post != s));

                startInstructions.Remove(valid);
                steps.RemoveAll(d => d.pre == valid);
                order.Add(valid);
            }
            
            

            return string.Join("", order);
        }
        
        public object Task2() {
            int timeSpent = 0;
            steps = input.Select(s => (s[5], s[36])).ToList();

            HashSet<char> instructions = new HashSet<char>();
            foreach (var tuple in steps) {
                instructions.Add(tuple.pre);
                instructions.Add(tuple.post);
            }

            instructions = instructions.OrderBy(x => x).ToHashSet();

            List<FuckEverything> working = new List<FuckEverything>();
            HashSet<char> buffer = new HashSet<char>();
            HashSet<char> done = new HashSet<char>();
            while (instructions.Any()) {
                foreach (char instruction in instructions) {
                    (char pre, char post) valueTuple = steps.FirstOrDefault(tuple => tuple.post == instruction && !done.Contains(tuple.pre));
                    if (valueTuple.IsDefault()) {
                        buffer.Add(instruction);
                    }
                }
                foreach (char c in buffer) {
                    instructions.Remove(c);
                }
                

                while (buffer.Any() && working.Count < 5) {
                    working.Add(new FuckEverything(buffer.First(),  buffer.First() - 'A' + 61));
                    buffer.Remove(buffer.First());
                }

                if (working.Any()) {
                    FuckEverything fastestInstr = working.OrderBy(tuple => tuple.Time).First();
                    timeSpent += fastestInstr.Time;
                    done.Add(fastestInstr.Instr);
                    int time = fastestInstr.Time;
                    foreach (var tuple in working)
                        tuple.Time -= time;

                    working.Remove(fastestInstr);
                }
            }
            


            return timeSpent;
        }
        
        private class FuckEverything {
            public char Instr { get; }
            public int Time { get; set; }

            public FuckEverything(char instr, int time) {
                Instr = instr;
                Time = time;
            }
        }

//        private void ProcessInstruction(HashSet<char> processableInstr, List<char> order) {
//            processableInstr.OrderBy(c => c).ToHashSet();
//            order.Add(processableInstr.First());
//            processableInstr.Remove(processableInstr.First());
//            
//            HashSet<char> buffer = new HashSet<char>();
//
//            foreach (var valueTuple in steps) {
//                if (valueTuple.pre == order.Last() && !order.Contains(valueTuple.post)) {
//                    bool breakFlag = false;
//                    foreach (var tuple in steps) {
//                        if (tuple.post == valueTuple.post && !order.Contains(tuple.pre)) {
//                            breakFlag = true;
//                            break;
//
//                        }
//
//                    }
//                    if(!breakFlag)
//                        buffer.Add(valueTuple.post);
//                }
//            }
//
//            processableInstr = processableInstr.Concat(buffer).ToHashSet();
//            if(processableInstr.Any())
//                ProcessInstruction(processableInstr, order);
//            
//        }

//        private void ProcessInstruction(char instr, List<char> order) {
//            if (steps != null) {
//                foreach ((char pre, char post) valueTuple in steps.Where(tuple => tuple.post == instr).OrderBy(tuple => tuple.pre)) {
//                    if(!order.Contains(valueTuple.pre))
//                        ProcessInstruction(valueTuple.pre, order);
//                }
//                order.Add(instr);
//            }
//        }

        
    }
}