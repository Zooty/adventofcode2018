namespace AdventOfCode2018.days {
    public interface IDay {
        int NumberOfDay { get; }
        
        object Task1();
        object Task2();
    }
}