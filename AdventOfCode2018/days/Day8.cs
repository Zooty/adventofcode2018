using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2018.days {
    public class Day8 : IDay {
        public int NumberOfDay { get; } = 8;
        private string[] input;

        public Day8(string[] input) {
            this.input = input;
        }

        public object Task1() {
            var numbers = input[0].Split(" ").Select(int.Parse).ToList();

            var i = 0;
            var node = Node.GetNode(numbers, ref i);

            return node.AllSum;
        }

        public object Task2() {
            var numbers = input[0].Split(" ").Select(int.Parse).ToList();

            var i = 0;
            var node = Node.GetNode(numbers, ref i);

            return node.Value;
        }
        
        
        
        private class Node
        {
            public List<Node> Nodes { get; } = new List<Node>();
            public List<int> Metadata { get; } = new List<int>();

            public int AllSum => Metadata.Sum() + Nodes.Sum(x => x.AllSum);

            public int Sum => Metadata.Sum();

            public int Value {
                get {
                    if (!Nodes.Any())
                        return Sum;

                    var value = 0;
                    foreach (int metadata in Metadata) {
                        if (metadata <= Nodes.Count) {
                            value += Nodes[metadata - 1].Value;
                        }
                    }

                    return value;
                }
            }

            public static Node GetNode(List<int> numbers, ref int i)
            {
                var node = new Node();

                int children = numbers[i++];
                int metadata = numbers[i++];

                for (var j = 0; j < children; j++)
                {
                    node.Nodes.Add(GetNode(numbers, ref i));
                }

                for (var j = 0; j < metadata; j++)
                {
                    node.Metadata.Add(numbers[i++]);
                }

                return node;
            }
        }
    }
}