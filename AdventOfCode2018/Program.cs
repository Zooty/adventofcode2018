﻿using System;
using System.IO;
using System.Linq;
using AdventOfCode2018.days;

namespace AdventOfCode2018 {
	internal static class Program {
		private static void Main(string[] args) {
			if (args.Length == 0) {
				Console.Out.WriteLine("Arguments needed!");
				Console.Out.WriteLine("Use `dotnet run <numbers of days>`");
				Environment.Exit(-1);
			}
			var type = typeof(IDay);
			var types = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(p => type.IsAssignableFrom(p) && !p.IsInterface);

			var dayTypes = types as Type[] ?? types.ToArray();
			foreach (var arg in args) {
				foreach (var dayType in dayTypes) {
					try {
						var dayNumber = int.Parse(arg);
						if (dayNumber == int.Parse(dayType.Name.Substring(3))) {
							if (dayType.GetConstructors()[0].Invoke(new object[] {
								File.ReadAllLines($"input\\day{dayNumber}.txt")
							}) is IDay day) {
								Console.Out.WriteLine($"\n---------------Day{day.NumberOfDay}---------------");
								try {
									Console.Out.Write($"Day{day.NumberOfDay}'s first task result: ");
									Console.Out.WriteLine(day.Task1());
								}
								catch (Exception e) {
									Console.Out.WriteLine();
									Console.WriteLine(e);
								}
								try {
									Console.Out.Write($"Day{day.NumberOfDay}'s second task result: ");
									Console.Out.WriteLine(day.Task2());
								}
								catch (Exception e) {
									Console.Out.WriteLine();
									Console.WriteLine(e);
								}
							}
						}
					}
					catch (Exception e) {
						Console.WriteLine(e);
					}
				}
			}
			Console.ReadKey();
		}
	}
}